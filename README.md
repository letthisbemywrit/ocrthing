###Ocr watcher

Watches for images on /public/download

Waits for the file to complete,

Runs OCR on the image

Emits an newFile event to socket.io

Socket.io sends the image url and the text that the ocr software got to
the browser

To use

```javascript

git clone https://bitbucket.org/letthisbemywrit/ocrthing.git

cd ocrthing

npm install

node server

//Go to http://serverip:3000/ on your browser

//Then in another console on the server

wget http://imgur.com/someimagewithtext.jpg

//wait some time for the image to download an ocr
//The image should be presented to you with the text

```
