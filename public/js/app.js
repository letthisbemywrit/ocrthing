$(document).ready(function(){
  console.log('document ready');
  socket = io.connect('/');
  socket.on('connect', function(){
    console.log('socket.io connected');
  });
  socket.on('newOcr', function(path, text){
    console.log('new ocr object', path, text);
    $("#image").html("<img src='/downloads/" + path + "'>");
    $("#imageText").html("<p>" + text );
  });
  socket.on('disconnect', function(){
    console.log("socket.io disconnected");
  });

});
