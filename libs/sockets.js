

//socket module gets an instance of server and the handle for events
module.exports = function(server, handle){
  //Make the socket.io websocket server attach to the server instance
  //Not actually listening yet
  var io = require('socket.io').listen(server);
  
  io.set('log level', 0);

  io.sockets.on('connection', function(socket){
    console.log('new connection ' + socket.id);

    socket.on('disconnect', function(){
      console.log('connection closed ' + socket.id);
    });

  });
  
  //Listen on events from the ocr module
  handle.on('newOcr', function(path, text) {
    console.log('got a new ocr event in socket.io', path, text);
    io.sockets.emit('newOcr', path, text);
  });


  

}
