var fs = require('fs');

var timeouts = {};
//module just gets a handle 
module.exports = function (handle){
  console.log('starting watchr');
  fs.watch(__dirname + '/../public/downloads/', function(event, filename){ 
    console.log('Change detected', event, filename);
    
    //Emit the change so the ocr part can take control of it 
    if(event == 'change'){
      //Change vents happen a lot so we set a timeout for when 
      //the file is done 
      //Check if there is a timeout already and clear it
      if(timeouts[filename]){
        clearTimeout(timeouts[filename]);
        delete timeouts[filename];
      }
      timeouts[filename] = setTimeout(function(){
        console.log('file completed... ' +  filename);
        handle.emit('newFile', filename);
        delete timeouts[filename];
      }, 1000);

    }

  });
}
