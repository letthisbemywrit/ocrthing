var nodecr = require('nodecr');

//module just gets the handle instance so it can listen for events from watchr
module.exports = function (handle){
  handle.on('newFile', function(path){
    console.log('ocr module got a new file ',  path);
    //Do some stuff
    nodecr.process(__dirname + '/../public/downloads/' + path, function(err, text){
      if(err){
        console.log('crap got an error with the ocr process');
      }else{
        //Emit a text event and let socket.io do the rest of the work
        handle.emit('newOcr', path, text);
      }
    });
  });
}
