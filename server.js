
/**
 * Module dependencies.
 */

var express = require('express')
  , app = express()
  , routes = require('./routes')
  , user = require('./routes/user')
  , server = require('http').createServer(app)
  , EventEmitter = require('events').EventEmitter
  , path = require('path');

//Event router for inner application data routing
var handle = new EventEmitter();

//Http server configuration
app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

//Set up main display routes
app.get('/', routes.index);
app.get('/users', user.list);

//Hook in the file watcher with the event handler and start the watch process
require('./libs/watch')(handle);
//Hook in the ocr scanner for the event handler
require('./libs/ocr')(handle);
//Hook in socket.io with the express server info and the event handler 
require('./libs/sockets')(server, handle);

//Start the http server
server.listen(app.get('port'), function(){
  console.log("node watchr-ocr server listening on port " + app.get('port'));
});
